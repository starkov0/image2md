//
//  ViewController.swift
//  image2MD
//
//  Created by Pierre Starkov on 10/07/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import UIKit
import AVFoundation
import GLKit

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate{
    let session = AVCaptureSession()
    var backCameraDevice: AVCaptureDevice!
    var frontCameraDevice: AVCaptureDevice!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var glContext: EAGLContext!
    var glView: GLKView!
    var ciContext: CIContext!
    var videoOutput: AVCaptureVideoDataOutput!
    var stillCameraOutput: AVCaptureStillImageOutput!
    var sessionQueue: dispatch_queue_t!
    
    override func viewDidLoad() {
        let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in availableCameraDevices as! [AVCaptureDevice] {
            if device.position == .Back {
                backCameraDevice = device
            }
            else if device.position == .Front {
                frontCameraDevice = device
            }
        }
        
        var error:NSError?
        let possibleCameraInput: AnyObject? = AVCaptureDeviceInput.deviceInputWithDevice(backCameraDevice, error: &error)
        if let backCameraInput = possibleCameraInput as? AVCaptureDeviceInput {
            if self.session.canAddInput(backCameraInput) {
                self.session.addInput(backCameraInput)
            }
        }
        
        previewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as! AVCaptureVideoPreviewLayer
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        
        glContext = EAGLContext(API: .OpenGLES2)
        glView = GLKView(frame: self.view.frame, context: glContext)
        ciContext = CIContext(EAGLContext: glContext)
        
//        videoOutput = AVCaptureVideoDataOutput()
//        videoOutput.setSampleBufferDelegate(self, queue: dispatch_queue_create("sample buffer delegate", DISPATCH_QUEUE_SERIAL))
//        if session.canAddOutput(self.videoOutput) {
//            session.addOutput(self.videoOutput)
//        }
        
        stillCameraOutput = AVCaptureStillImageOutput()
        if self.session.canAddOutput(self.stillCameraOutput) {
            self.session.addOutput(self.stillCameraOutput)
        }
        
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        sessionQueue = dispatch_queue_create("com.example.camera.capture_session", DISPATCH_QUEUE_SERIAL)
        dispatch_async(sessionQueue) { () -> Void in
            self.session.startRunning()
        }
        
        let focusMode:AVCaptureFocusMode = AVCaptureFocusMode.ContinuousAutoFocus
        if backCameraDevice.isFocusModeSupported(focusMode) {
            var error:NSError?
            if backCameraDevice.lockForConfiguration(&error) {
                backCameraDevice.focusMode = focusMode
                // locked successfully, go on with configuration
                // currentDevice.unlockForConfiguration()
            }
            else {
                // something went wrong, the device was probably already locked
            }
        }
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let image = CIImage(CVPixelBuffer: pixelBuffer)
        if glContext != EAGLContext.currentContext() {
            EAGLContext.setCurrentContext(glContext)
        }
        glView.bindDrawable()
        ciContext.drawImage(image, inRect:image.extent(), fromRect: image.extent())
        glView.display()
    }

    

}

